import titleNotificationService from './title-notification-service.js';

export default {
  notificationService: titleNotificationService()
};
