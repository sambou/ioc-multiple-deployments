let titleNotificationService = () => {
  let initialTitle = document.getElementsByTagName('title')[0].innerText;

  let service = {
    notify() {
      document.title =  '(1) ' + initialTitle;
    },

    cancelNotification() {
      document.title = initialTitle;
    }
  };

  return service;
};

export default titleNotificationService;
