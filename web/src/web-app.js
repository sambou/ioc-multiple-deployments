import iocContainer from './web-ioc-container';
import appFactory   from '../../app/app.js';

let app = appFactory(iocContainer);
app.render();
