var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: './src/web-app.js',
  output: {
    path: __dirname,
    filename: './dist/bundle.js'
  },
  module: {
      loaders: [
          { test: /\.js$/, loader: 'babel-loader' }
      ]
  },
  resolve: {
      root: [path.join(__dirname, 'libs')]
  },
  plugins: [

  ]
};
