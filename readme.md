# IoC for multiple deployments
Showcases how to develop an app that is decoupled from its host platform (e.g. web or desktop). Uses naive DI to inject concretions into the actual application. This approach is useful when you want to support multiple platforms (web, desktop) without rewriting the actual application.

## Problem
Consider implementing a chat app with a web and desktop version. How do you notify the user and use appropriate APIs (dock, DOM title) without rewriting the whole app?

![Dock](https://github.com/sambou/adapter-app/blob/master/static/dock.png)
![DOM Title](https://github.com/sambou/adapter-app/blob/master/static/web.png)
## Requirements
- the app should provide a way to notify a user
- on the web platform, this is done via changing the DOM title
- on the desktop platform this is done via setting a badge on the dock icon (or a system beep)

## Running it
- run npm install from desktop/ and web/ to set up dependencies
- run webpack (or webpack --watch) from /web
- open web/index.html for web-based app
- run electron desktop/electron-app.js for electron-based app
