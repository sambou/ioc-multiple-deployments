import badgeNotificationService from './services/badge-notification-service';
import beepNotificationService from './services/beep-notification-service';

export default {
  notificationService: badgeNotificationService()
};
