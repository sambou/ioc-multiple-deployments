import ipc from 'ipc';

let badgeNotificationService = () => {
  let service = {
    notify() {
      ipc.send('badge-notify', '1');
    },

    cancelNotification() {
      ipc.send('badge-cancelNotification', '');
    }
  };

  return service;
};

export default badgeNotificationService;
