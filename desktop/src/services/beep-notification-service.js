import shell from 'shell';

let beepNotificationService = () => {
  let service = {
    notify() {
      shell.beep();
    },

    cancelNotification() {
      shell.beep();
    }
  };

  return service;
};

export default beepNotificationService;
