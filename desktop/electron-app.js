'use babel';

var app = require('app');
var BrowserWindow = require('browser-window');
var mainWindow = null;
var ipc = require('ipc');

app.on('window-all-closed', function() {
  if (process.platform != 'darwin') {
    app.quit();
  }
});

app.on('ready', function() {

  mainWindow = new BrowserWindow({width: 800, height: 600});

  mainWindow.loadUrl('file://' + __dirname + '/index.html');
  mainWindow.setTitle('Fancy Notifications');

  mainWindow.on('closed', function() {
    mainWindow = null;
  });

});

ipc.on('badge-notify', function(event, arg) {
  app.dock.setBadge(arg);
});

ipc.on('badge-cancelNotification', function(event, arg) {
  app.dock.setBadge(arg);
});
