let appFactory = (iocContainer) => {
  let notificationService = iocContainer.notificationService;

  var app = {
    notify() {
      notificationService.notify();
    },
    cancelNotification() {
      notificationService.cancelNotification();
    },
    render() {
      document.getElementById('app-hook').innerHTML =
        '<div class="container">' +
          '<a class="item" id="notify" href="#notify"> Notify </a>' +
          '<a class="item" id="cancel" href="#cancel"> Cancel </a>' +
        '</div>';

      document.getElementById('notify').onclick = app.notify;
      document.getElementById('cancel').onclick = app.cancelNotification;
    }
  };

  return app;
};

export default appFactory;
